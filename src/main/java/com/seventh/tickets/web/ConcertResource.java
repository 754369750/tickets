package com.seventh.tickets.web;

import com.seventh.tickets.entity.Concert;
import com.seventh.tickets.service.ConcertService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ConcertResource {

    private final ConcertService concertService;

    public ConcertResource(ConcertService concertService){
        this.concertService = concertService;
    }


    @ApiOperation(value="新增演唱会信息")
    @PostMapping("/addconcert")
    public ResponseEntity<Concert> concertInsert(@RequestParam String concertname,
                                                 @RequestParam String singer,
                                                 @RequestParam String concertdate,
                                                 @RequestParam String concertstarttime,
                                                 @RequestParam String concertplace,
                                                 @RequestParam String grade_1,
                                                 @RequestParam String price_1,
                                                 @RequestParam String seat_1,
                                                 @RequestParam String grade_2,
                                                 @RequestParam String price_2,
                                                 @RequestParam String seat_2,
                                                 @RequestParam String grade_3,
                                                 @RequestParam String price_3,
                                                 @RequestParam String seat_3
                                                 ) throws Exception {

        Concert concert = concertService.insertConcert(concertname,singer,concertdate,concertstarttime,concertplace,grade_1,price_1,seat_1,grade_2,price_2,seat_2,grade_3,price_3,seat_3);
        if (concert!=null ){
            return ResponseEntity.ok().body(concert);
        }else {
            return new ResponseEntity<Concert>(HttpStatus.BAD_REQUEST);
        }
    }



    @ApiOperation(value="修改演唱会信息")
    @PutMapping("/userdateconcert")
    public ResponseEntity<Concert> updateConcert(@RequestParam Long concertid,
                                                 @RequestParam String concertname,
                                                 @RequestParam String singer,
                                                 @RequestParam String concertdate,
                                                 @RequestParam String concertstarttime,
                                                 @RequestParam String concertplace) throws Exception {

        Concert concert = concertService.updateConcert(concertid,concertname,singer,concertdate,concertstarttime,concertplace);
        if (concert!=null){
            return ResponseEntity.ok().body(concert);
        }else {
            return new ResponseEntity<Concert>(HttpStatus.BAD_REQUEST);
        }
    }


    @ApiOperation(value="删除演唱会信息")
    @PostMapping("/deldeteconcert")
    public ResponseEntity<String> deleteConcert(@RequestParam String concert_id) throws Exception {

        System.out.println(concert_id);
        if (concert_id!=null){
          String status =  concertService.deleteConcert(Long.parseLong(concert_id));
            return ResponseEntity.ok().body(status);
        }else {
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value="关键字查询演唱会信息")
    @GetMapping("/keywordsconcert")
    public ResponseEntity<List<Concert>> findkeywordsConcert(@RequestParam String keywords) throws Exception {

        if (keywords!=null){
            List<Concert> concerts = concertService.findBykeywords(keywords);
            return ResponseEntity.ok().body(concerts);
        }else {
            return new ResponseEntity<List<Concert>>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value="查询所有演唱会信息")
    @GetMapping("/allconcert")
    public ResponseEntity<List<Concert>> findallConcert() throws Exception {
        List<Concert> concerts = concertService.findAllConcerts();
        if (concerts.size()>0){
            return ResponseEntity.ok().body(concerts);
        }else {
            return new ResponseEntity<List<Concert>>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value="根据演唱会id查询信息")
    @GetMapping("/idconcert")
    public ResponseEntity<Concert> findidConcert(@RequestParam Long concertid) throws Exception {
        Concert  concert = concertService.findByConcertid(concertid);
        if (concert!=null){
            return ResponseEntity.ok().body(concert);
        }else {
            return new ResponseEntity<Concert>(HttpStatus.BAD_REQUEST);
        }
    }





}
