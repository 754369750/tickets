package com.seventh.tickets.web;

import com.seventh.tickets.entity.User;
import com.seventh.tickets.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class UserResource {

    private  final UserService userService;
    public UserResource (UserService userService){

        this.userService = userService;
    }

    @ApiOperation(value="用户登录")
    @PostMapping("/userlogin")
    public ResponseEntity<String> userLogin(@RequestParam String userloginname, @RequestParam String password) throws Exception {
        if (userloginname!=null && password!=null){
            String  user = userService.userLogin(userloginname,password);
            return ResponseEntity.ok().body(user);
        }else {
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value="用户注册")
    @PostMapping("/userregister")
    public ResponseEntity<User> userRegister(@RequestParam String userloginname, @RequestParam String password) throws Exception {
        if (userloginname!=null && password!=null){
            User  user = userService.userRegister(userloginname,password);
                return ResponseEntity.ok().body(user);
        }else {
            return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
        }
    }


}
