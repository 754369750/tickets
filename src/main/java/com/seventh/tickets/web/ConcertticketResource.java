package com.seventh.tickets.web;

import com.seventh.tickets.entity.Concertticket;
import com.seventh.tickets.service.ConcertticketService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ConcertticketResource {

    private  final ConcertticketService concertticketService;
    public  ConcertticketResource (ConcertticketService concertticketService){

        this.concertticketService = concertticketService;
    }

    @ApiOperation(value="根据演唱会id查询所有票务信息")
    @GetMapping("/idconcertticket")
    public ResponseEntity<List<Concertticket>> findByidConcerttickerts(@RequestParam Long concertid) throws Exception {
      List<Concertticket> concerttickets = concertticketService.findByConcertid(concertid);
        if (concerttickets.size()!=0){
            return ResponseEntity.ok().body(concerttickets);
        }else {
            return new ResponseEntity<List<Concertticket>>(HttpStatus.BAD_REQUEST);
        }
    }

}
