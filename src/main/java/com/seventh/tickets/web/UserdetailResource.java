package com.seventh.tickets.web;

import com.seventh.tickets.entity.Userdetail;
import com.seventh.tickets.service.UserdetailService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UserdetailResource {

    private  final UserdetailService userdetailService;
    public UserdetailResource (UserdetailService userdetailService){

        this.userdetailService = userdetailService;
    }

    @ApiOperation(value="获取用户信息")
    @PostMapping("/userinfo")
    public ResponseEntity<Userdetail> getUserinfo(@RequestParam String userloginname) throws Exception {
        if (userloginname!=null){
           Userdetail userdetail = userdetailService.getUserdetail(userloginname);
            return ResponseEntity.ok().body(userdetail);
        }else {
            return new ResponseEntity<Userdetail>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value="修改用户信息")
    @PutMapping("/userdateuserinfo")
    public ResponseEntity<Userdetail> updateUserinfo(@RequestParam String userloginname,
                                                     @RequestParam String username,
                                                     @RequestParam String userphone,
                                                     @RequestParam String userliveplace,
                                                     @RequestParam String useremail,
                                                     @RequestParam String useridcard) throws Exception {
        Userdetail userdetail = userdetailService.updateUserdetail(userloginname,username,userphone,userliveplace,useremail,useridcard);
        if (userdetail!=null){
            return ResponseEntity.ok().body(userdetail);
        }else {
            return new ResponseEntity<Userdetail>(HttpStatus.BAD_REQUEST);
        }
    }





}
