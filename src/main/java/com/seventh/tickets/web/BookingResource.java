package com.seventh.tickets.web;

import com.seventh.tickets.entity.Booking;
import com.seventh.tickets.service.BookingService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class BookingResource {

    private  final BookingService bookingService;
    public BookingResource (BookingService bookingService){
        this.bookingService = bookingService;
    }

    @ApiOperation(value="用户购票")
    @PostMapping("/addbooking")
    public ResponseEntity<Booking> bookingInsert(@RequestParam Long concertid,
                                                 @RequestParam String useridcard,
                                                 @RequestParam String seattype) throws Exception {

        Booking booking = bookingService.insertBooking(concertid,useridcard,seattype);
        if (booking!=null ){
            return ResponseEntity.ok().body(booking);
        }else {
            return new ResponseEntity<Booking>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value="根据身份证号查询演唱会购票信息")
    @GetMapping("/idcardbooking")
    public ResponseEntity<List<Booking>> finduseridcard(@RequestParam String userid_card) throws Exception {
        if (userid_card!=null){
            List<Booking> bookings = bookingService.findByUseridcard(userid_card);
            return ResponseEntity.ok().body(bookings);
        }else {
            return new ResponseEntity<List<Booking>>(HttpStatus.BAD_REQUEST);
        }
    }




}
