package com.seventh.tickets.service.Impl;


import com.seventh.tickets.entity.Userdetail;
import com.seventh.tickets.repository.UserdetailRepository;
import com.seventh.tickets.service.UserdetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserdetailServiceImpl implements UserdetailService {


    private  final UserdetailRepository userdetailRepository;
    public UserdetailServiceImpl (UserdetailRepository userdetailRepository){

        this.userdetailRepository = userdetailRepository;
    }
    @Override
    public Userdetail getUserdetail(String userloginname) {
        return userdetailRepository.findByUserloginname(userloginname);
    }


    @Override
    public Userdetail updateUserdetail(String userloginname, String username, String userphone, String userliveplace, String useremail, String useridcard) throws Exception {

        if (userloginname ==null || username==null || userphone==null || userliveplace==null || useremail==null|| useridcard==null){
            throw new Exception("修改用户信息数据不完整");
        }else {
            Userdetail userdetail = userdetailRepository.findByUserloginname(userloginname);
            if (userdetail==null){
                throw new Exception("未查到该用户信息");
            }else {
                userdetail.setUsername(username);
                userdetail.setUserphone(userphone);
                userdetail.setUserliveplace(userliveplace);
                userdetail.setUseremail(useremail);
                userdetail.setUseridcard(useridcard);
                Userdetail  userdetail1 = userdetailRepository.save(userdetail);
                if (userdetail1.getDetailid()>0){
                    return  userdetail1;
                }else {
                    throw new Exception("修改用户信息失败！");
                }
            }
        }
    }
}
