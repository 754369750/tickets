package com.seventh.tickets.service.Impl;

import com.seventh.tickets.entity.Concert;
import com.seventh.tickets.entity.Concertticket;
import com.seventh.tickets.repository.ConcertRepository;
import com.seventh.tickets.repository.ConcertticketRepository;
import com.seventh.tickets.service.ConcertService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ConcertServiceImpl implements ConcertService {

    private final ConcertRepository concertRepository;
    private final ConcertticketRepository concertticketRepository;
    public  ConcertServiceImpl (ConcertRepository concertRepository,
                                ConcertticketRepository concertticketRepository ){
        this.concertRepository = concertRepository;
        this.concertticketRepository = concertticketRepository;
    }

    @Override
    public Concert insertConcert(String concert_name, String singer, String concert_date, String concert_starttime, String concert_place, String grade_1, String price_1, String seat_1, String grade_2, String price_2, String seat_2, String grade_3, String price_3, String seat_3) throws Exception {

        if (concert_name == null || singer == null || concert_date==null ||concert_starttime ==null || concert_place == null || grade_1==null || price_1==null|| seat_1==null || grade_2==null || price_2==null|| seat_2==null || grade_3==null || price_3==null|| seat_3==null){
            throw new Exception("新增演唱会信息失败，数据不完整");
        }else {
            Concert concert = concertRepository.findByConcertnameAndConcertdate(concert_name,concert_date);
            if (concert!=null){
                throw new Exception("新增演唱会信息失败,已经存在此演唱会");
            }else {
                Concert concert1 = new Concert();
                concert1.setConcertname(concert_name);
                concert1.setSinger(singer);
                concert1.setConcertdate(concert_date);
                concert1.setConcertstarttime(concert_starttime);
                concert1.setConcertplace(concert_place);
                concert1 = concertRepository.save(concert1);
                System.out.println(concert1);
                if (concert1.getConcertid()>0){
                    Concertticket concertticket = new Concertticket();
                    System.out.println(concert1.getConcertid());
                    concertticket.setConcertid(concert1.getConcertid());
                    concertticket.setSeatgrade(grade_1);
                    concertticket.setSeatprice(price_1);
                    concertticket.setSeattotal(Integer.parseInt(seat_1));
                    concertticket.setSeatleft(Integer.parseInt(seat_1));
                    concertticket = concertticketRepository.save(concertticket);

                    Concertticket concertticket2 = new Concertticket();
                    concertticket2.setConcertid(concert1.getConcertid());
                    concertticket2.setSeatgrade(grade_2);
                    concertticket2.setSeatprice(price_2);
                    concertticket2.setSeattotal(Integer.parseInt(seat_2));
                    concertticket2.setSeatleft(Integer.parseInt(seat_2));
                    concertticket2 = concertticketRepository.save(concertticket2);

                    Concertticket concertticket3 = new Concertticket();
                    concertticket3.setConcertid(concert1.getConcertid());
                    concertticket3.setSeatgrade(grade_3);
                    concertticket3.setSeatprice(price_3);
                    concertticket3.setSeattotal(Integer.parseInt(seat_3));
                    concertticket3.setSeatleft(Integer.parseInt(seat_3));
                    concertticket3 = concertticketRepository.save(concertticket3);

                    if (concertticket.getConcertid()>0 && concertticket2.getConcertid()>0 && concertticket3.getConcertid()>0 ){
                        return  concert1;
                    }else {
                        throw new Exception("票务详细信息插入失败!");
                    }
                }else {
                    throw new Exception("插入失败!");
                }
            }
        }

    }

    @Override
    public Concert updateConcert(Long concert_id, String concert_name, String singer, String concert_date, String concert_starttime, String concert_place) throws Exception {

        Concert concert = concertRepository.findOne(concert_id);
        concert.setConcertname(concert_name);
        concert.setSinger(singer);
        concert.setConcertdate(concert_date);
        concert.setConcertstarttime(concert_starttime);
        concert.setConcertplace(concert_place);
        concert = concertRepository.save(concert);
        if (concert.getConcertid()>0){
            return  concert;

        }else {
            throw new Exception("演唱会信息修改失败!");
        }

    }

    @Override
    public String deleteConcert(Long concert_id) throws Exception {
        Concert concert = concertRepository.findOne(concert_id);
        if (concert==null){
            throw new Exception("未查询到该演唱会信息");
        }else {
            List<Concertticket> concerttickets = concertticketRepository.findAllByConcertid(concert.getConcertid());
            if (concerttickets.size()>0){
                for ( int i=0;i<concerttickets.size();i++){
                    concertticketRepository.delete(concerttickets.get(i).getTicketid());
                }
                concertRepository.delete(concert_id);
                return "0";
            }else {
                throw new Exception("未查询到该演唱会票务信息");
            }
        }

    }

    @Override
    public List<Concert> findAllConcerts() {
        return concertRepository.findAll();
    }

    @Override
    public List<Concert> findBykeywords(String keywords) throws Exception {
        if (keywords==null){
            throw new Exception("查询条件不存在");
        }else {
            return concertRepository.findBykeywords(keywords);
        }

    }

    @Override
    public Concert findByConcertid(Long concert_id) {
        return concertRepository.findOne(concert_id);
    }


}
