package com.seventh.tickets.service.Impl;

import com.seventh.tickets.entity.Concertticket;
import com.seventh.tickets.repository.ConcertticketRepository;
import com.seventh.tickets.service.ConcertticketService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ConcertticketServiceImpl implements ConcertticketService {

    private final ConcertticketRepository concertticketRepository;
    public ConcertticketServiceImpl( ConcertticketRepository concertticketRepository){

        this.concertticketRepository = concertticketRepository;
    }

    @Override
    public List<Concertticket> findByConcertid(Long concert_id) {
        return concertticketRepository.findAllByConcertid(concert_id);
    }

    @Override
    public Concertticket findPrice(Long concert_id, String grade) {
        return concertticketRepository.findByConcertidAndSeatgrade(concert_id,grade);
    }
}
