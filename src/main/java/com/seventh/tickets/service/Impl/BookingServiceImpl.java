package com.seventh.tickets.service.Impl;

import com.seventh.tickets.entity.Booking;
import com.seventh.tickets.entity.Concert;
import com.seventh.tickets.entity.Concertticket;
import com.seventh.tickets.repository.BookingRepository;
import com.seventh.tickets.repository.ConcertRepository;
import com.seventh.tickets.repository.ConcertticketRepository;
import com.seventh.tickets.service.BookingService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class BookingServiceImpl implements BookingService {

    private  final BookingRepository bookingRepository;
    private  final ConcertRepository concertRepository;
    private  final ConcertticketRepository concertticketRepository;
    public  BookingServiceImpl(BookingRepository bookingRepository,ConcertRepository concertRepository,ConcertticketRepository concertticketRepository){
        this.bookingRepository = bookingRepository;
        this.concertRepository = concertRepository;
        this.concertticketRepository = concertticketRepository;
    }

    @Override
    public Booking insertBooking(Long concert_id, String useridcard, String seattype) throws Exception {

        if (concert_id==null|| useridcard==null|| seattype==null){
            throw new Exception("数据不完整，购票失败");

        }else {
            Concert concert = concertRepository.findOne(concert_id);
            if (concert==null){
                throw new Exception("错误，未查到该演唱会信息，演唱会id为"+concert_id);
            }else {
                Concertticket concertticket1 = concertticketRepository.findByConcertidAndSeatgrade(concert_id,seattype);
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
                String date = df.format(new Date());
                Booking booking = new Booking();
                booking.setConcert(concert);
                booking.setUseridcard(useridcard);
                booking.setSeattype(seattype);
                booking.setBookingprice(concertticket1.getSeatprice());
                booking.setBookingtime(date);
                booking = bookingRepository.save(booking);
                if (booking.getBookingid()>0){
                    Concertticket concertticket = concertticketRepository.findByConcertidAndSeatgrade(concert_id,seattype);
                    if (concertticket==null){
                        throw new Exception("未查询到该演唱会"+concert_id+"，座位类别为"+seattype+"的信息");
                    }else {
                        int seatleft = concertticket.getSeatleft();
                        if (seatleft>0){
                            concertticket.setSeatleft(concertticket.getSeatleft()-1);
                            concertticket = concertticketRepository.save(concertticket);
                            if (concertticket.getConcertid()>0){
                                return booking;
                            }else{
                                throw new Exception("用户购票，座位剩余数量-1失败!");
                            }
                        }else {
                            bookingRepository.delete(booking.getBookingid());
                            Booking booking1 = new Booking();
                            booking1.setBookingtime("该场次已经售完");
                            return  booking1;
                        }
                    }
                }else {
                    throw new Exception("保存购买票失败，演唱会id为"+concert_id);
                }
            }
        }

    }

    @Override
    public List<Booking> findByUseridcard(String useridcard) {
        return bookingRepository.findAllByIdcard(useridcard);
    }
}
