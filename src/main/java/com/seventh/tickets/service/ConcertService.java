package com.seventh.tickets.service;

import com.seventh.tickets.entity.Concert;

import java.util.List;

public interface ConcertService {

    Concert insertConcert(String concert_name,String singer,String concert_date,String concert_starttime,String concert_place,String grade_1,String price_1,String seat_1,String grade_2,String price_2,String seat_2,String grade_3,String price_3,String seat_3) throws Exception;

    Concert updateConcert(Long concert_id ,String concert_name,String singer,String concert_date,String concert_starttime,String concert_place) throws Exception;

    String deleteConcert(Long concert_id) throws Exception;

    List<Concert> findAllConcerts();

    List<Concert> findBykeywords(String keywords) throws Exception;

    Concert findByConcertid(Long concert_id );

}
