package com.seventh.tickets.service;

import com.seventh.tickets.entity.Userdetail;

public interface UserdetailService {

    Userdetail getUserdetail(String userloginname);

    Userdetail updateUserdetail(String userloginname,String username,String userphone,String userliveplace,String useremail,String useridcard) throws Exception;


}
