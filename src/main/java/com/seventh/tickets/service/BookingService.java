package com.seventh.tickets.service;

import com.seventh.tickets.entity.Booking;
import com.seventh.tickets.entity.Concert;

import java.util.List;

public interface BookingService {

    Booking insertBooking(Long concert_id,String useridcard,String seattype) throws Exception;


    List<Booking> findByUseridcard(String useridcard);

}
