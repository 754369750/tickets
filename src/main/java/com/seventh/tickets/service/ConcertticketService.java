package com.seventh.tickets.service;

import com.seventh.tickets.entity.Concertticket;

import java.util.List;

public interface ConcertticketService {

    List<Concertticket> findByConcertid(Long concert_id);

    Concertticket findPrice(Long concert_id,String grade);
}
