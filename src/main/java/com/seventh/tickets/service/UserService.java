package com.seventh.tickets.service;

import com.seventh.tickets.entity.User;

public interface UserService {

    String userLogin (String userloginname,String password) throws Exception;

    User userRegister(String userloginname,String password) throws Exception;



}
