package com.seventh.tickets.repository;

import com.seventh.tickets.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    User findByUserloginname(String userloginname);
}
