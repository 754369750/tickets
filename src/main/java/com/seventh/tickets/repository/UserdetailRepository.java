package com.seventh.tickets.repository;

import com.seventh.tickets.entity.Userdetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface UserdetailRepository extends JpaRepository<Userdetail,Long> {

    Userdetail findByUserloginname (String userloginname);
}
