package com.seventh.tickets.repository;

import com.seventh.tickets.entity.Concertticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@SuppressWarnings("unused")
@Repository
public interface ConcertticketRepository extends JpaRepository<Concertticket,Long> {

    @Query("select a from Concertticket a where a.concertid=?1")
    List<Concertticket> findAllByConcertid(Long concertid);

    Concertticket findByConcertidAndSeatgrade(Long concertid,String seatgrade);



}
