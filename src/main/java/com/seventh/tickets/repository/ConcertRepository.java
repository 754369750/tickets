package com.seventh.tickets.repository;

import com.seventh.tickets.entity.Concert;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@SuppressWarnings("unused")
@Repository
public interface ConcertRepository  extends JpaRepository<Concert,Long> {

    Concert findByConcertnameAndConcertdate(String concertname,String concertdate);

    @Query("select a from Concert a where" +
            " (a.concertname like concat('%', ?1, '%') " +
            "or a.concertplace like concat('%', ?1, '%') " +
            "or a.concertdate like concat('%', ?1, '%') " +
            "or a.concertstarttime like concat('%', ?1, '%') " +
            "or a.singer like concat('%', ?1, '%') )")
    List<Concert> findBykeywords(String keywords);
}
