package com.seventh.tickets.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@javax.persistence.Table(name = "concert")
public class Concert implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long  concert_id;

    @Column(name = "concert_name")
    private String concertname;

    @Column(name = "concert_place")
    private String concertplace;

    @Column(name = "concert_date")
    private String concertdate;

    @Column(name = "concert_starttime")
    private String concertstarttime;

    @Column(name = "singer")
    private String singer;

    public Long getConcertid() {
        return concert_id;
    }

    public void setConcertid(Long concertid) {
        this.concert_id = concertid;
    }

    public String getConcertname() {
        return concertname;
    }

    public void setConcertname(String concertname) {
        this.concertname = concertname;
    }

    public String getConcertplace() {
        return concertplace;
    }

    public void setConcertplace(String concertplace) {
        this.concertplace = concertplace;
    }

    public String getConcertdate() {
        return concertdate;
    }

    public void setConcertdate(String concertdate) {
        this.concertdate = concertdate;
    }

    public String getConcertstarttime() {
        return concertstarttime;
    }

    public void setConcertstarttime(String concertstarttime) {
        this.concertstarttime = concertstarttime;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    @Override
    public String toString() {
        return "Concert{" +
                "concert_id=" + concert_id +
                ", concertname='" + concertname + '\'' +
                ", concertplace='" + concertplace + '\'' +
                ", concertdate='" + concertdate + '\'' +
                ", concertstarttime='" + concertstarttime + '\'' +
                ", singer='" + singer + '\'' +
                '}';
    }
}
