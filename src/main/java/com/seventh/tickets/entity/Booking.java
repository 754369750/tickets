package com.seventh.tickets.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@javax.persistence.Table(name = "booking")
public class Booking implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long booking_id;

    @ManyToOne
    @JoinColumn(name="concert_id",referencedColumnName = "concert_id")
    private Concert concert;

    @Column(name = "user_idcard")
    private String useridcard;

    @Column(name = "seat_type")
    private String seattype;

    @Column(name = "booking_price")
    private String bookingprice;

    @Column(name = "booking_time")
    private String bookingtime;

    public Long getBookingid() {
        return booking_id;
    }

    public void setBookingid(Long bookingid) {
        this.booking_id = bookingid;
    }

    public Concert getConcert() {
        return concert;
    }

    public void setConcert(Concert concert) {
        this.concert = concert;
    }

    public String getUseridcard() {
        return useridcard;
    }

    public void setUseridcard(String useridcard) {
        this.useridcard = useridcard;
    }

    public String getSeattype() {
        return seattype;
    }

    public void setSeattype(String seattype) {
        this.seattype = seattype;
    }

    public String getBookingprice() {
        return bookingprice;
    }

    public void setBookingprice(String bookingprice) {
        this.bookingprice = bookingprice;
    }

    public String getBookingtime() {
        return bookingtime;
    }

    public void setBookingtime(String bookingtime) {
        this.bookingtime = bookingtime;
    }
}
