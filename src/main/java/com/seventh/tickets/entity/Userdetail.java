package com.seventh.tickets.entity;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@javax.persistence.Table(name = "userdetail")
public class Userdetail implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long detail_id;

    @Column(name = "user_loginname")
    private String userloginname;
    @Column(name = "user_name")
    private String username;
    @Column(name = "user_phone")
    private String userphone;
    @Column(name = "user_liveplace")
    private String userliveplace;
    @Column(name = "user_email")
    private String useremail;
    @Column(name = "user_idcard")
    private String useridcard;

    public Long getDetailid() {
        return detail_id;
    }

    public void setDetailid(Long detailid) {
        this.detail_id = detailid;
    }

    public String getUserloginname() {
        return userloginname;
    }

    public void setUserloginname(String userloginname) {
        this.userloginname = userloginname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserphone() {
        return userphone;
    }

    public void setUserphone(String userphone) {
        this.userphone = userphone;
    }

    public String getUserliveplace() {
        return userliveplace;
    }

    public void setUserliveplace(String userliveplace) {
        this.userliveplace = userliveplace;
    }

    public String getUseremail() {
        return useremail;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }

    public String getUseridcard() {
        return useridcard;
    }

    public void setUseridcard(String useridcard) {
        this.useridcard = useridcard;
    }
}
