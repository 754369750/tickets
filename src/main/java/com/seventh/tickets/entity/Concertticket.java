package com.seventh.tickets.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@javax.persistence.Table(name = "concert_ticket")
public class Concertticket implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ticket_id;

    @Column(name = "concert_id")
    private Long concertid;

    @Column(name = "seat_grade")
    private String seatgrade;

    @Column(name = "seat_price")
    private String seatprice;

    @Column(name = "seat_total")
    private int seattotal;

    @Column(name = "seat_left")
    private int seatleft;

    public Long getTicketid() {
        return ticket_id;
    }

    public void setTicketid(Long ticketid) {
        this.ticket_id = ticketid;
    }

    public Long getConcertid() {
        return concertid;
    }

    public void setConcertid(Long concertid) {
        this.concertid = concertid;
    }

    public String getSeatgrade() {
        return seatgrade;
    }

    public void setSeatgrade(String seatgrade) {
        this.seatgrade = seatgrade;
    }

    public String getSeatprice() {
        return seatprice;
    }

    public void setSeatprice(String seatprice) {
        this.seatprice = seatprice;
    }

    public int getSeattotal() {
        return seattotal;
    }

    public void setSeattotal(int seattotal) {
        this.seattotal = seattotal;
    }

    public int getSeatleft() {
        return seatleft;
    }

    public void setSeatleft(int seatleft) {
        this.seatleft = seatleft;
    }
}
