(function($) {
    'use strict';

    $(function() {
       $("#subBtn").click(function () {
           var loginname = $("#doc-ipt-email-1").val();
           var password = $("#doc-ipt-pwd-1").val();
           $.ajax({
               type: "POST",
               url: "/api/userlogin",    //向后端请求数据的url
               data: {
                   userloginname:loginname,
                   password:password
               },
               success: function (data) {
                  console.log(data);
                  if (data=="0"){
                      $.ajax({
                          type: "POST",
                          url: "/api/userinfo",    //向后端请求数据的url
                          data: {
                              userloginname:loginname
                          },
                          success: function (data) {
                              setCookie("loginname",data.userloginname,"","","","");
                              setCookie("username",data.username,"","","","");
                              window.location.href="userindex.html";
                          }
                      });


                  }else if (data=="1"){
                      $('#none-modal').modal('open');
                  }else if (data=="2"){
                      $('#error-modal').modal('open');
                  }
               }
           });
       });

        function setCookie(name,value,expires,path,domain,secure){
            document.cookie=name+"="+encodeURI(value)+
                ((expires) ? "; expires=" + expires : "")+
                ((path) ? "; path=" + path : "")+
                ((domain) ? "; domain=" + domain : "")+
                ((secure) ? "; secure=" + secure : "");
        }

        //获取浏览器中的Cookie
        function getCookie(cName){
            var cookieString = decodeURI(document.cookie);
            var cookieArray = cookieString.split("; ");
            // console.log(cookieArray.length);
            for(var i = 0; i < cookieArray.length; i++){
                var cookieNum = cookieArray[i].split("=");
                // console.log(cookieNum.toString());
                var cookieName = cookieNum[0];
                var cookieValue = cookieNum[1];

                if(cookieName == cName){
                    return cookieValue;
                }
            }
            return false;
        }

    });

})(jQuery);
