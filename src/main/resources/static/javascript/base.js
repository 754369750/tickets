(function($) {
    'use strict';
    $(document).ready(function(){

        $("#header").load('../public/header.html');

       var loginname = getCookie("loginname");
       var username = getCookie("username");
       if (username=="管理员"){
           $("#sidebar").load('../public/adminsidebar.html');
       }else {
           $("#sidebar").load('../public/sidebar.html');
       }

    });

    //获取浏览器中的Cookie
    function getCookie(cName){
        var cookieString = decodeURI(document.cookie);
        var cookieArray = cookieString.split("; ");
        // console.log(cookieArray.length);
        for(var i = 0; i < cookieArray.length; i++){
            var cookieNum = cookieArray[i].split("=");
            // console.log(cookieNum.toString());
            var cookieName = cookieNum[0];
            var cookieValue = cookieNum[1];

            if(cookieName == cName){
                return cookieValue;
            }
        }
        return false;
    }


})(jQuery);
