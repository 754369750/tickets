(function($) {
    'use strict';
    $(document).ready(function(){

        $("#header").load('../public/header.html');

        var loginname = getCookie("loginname");
        var username = getCookie("username");
        if (username=="管理员"){
            $("#sidebar").load('../public/adminsidebar.html');
        }else {
            $("#sidebar").load('../public/sidebar.html');
        }

        initForm();

        $(".searchBtn").click(function () {
            var keywords = $("#searchtext").val();
            $.ajax({
                type: "GET",
                url: "/api/keywordsconcert",    //向后端请求数据的url
                data: {
                    keywords:keywords
                },
                success: function (data) {
                    if (data.length>0){
                        showForm(data);
                    }
                }
            });
            
        });

      function initForm() {
        $.ajax({
        type: "GET",
        url: "/api/allconcert",    //向后端请求数据的url
        data: {},
        success: function (data) {
            if (data.length>0){
                showForm(data);
               }

              }
           });
      }
      
      function deleteConcert(concert_id) {
          console.log(concert_id)
          $.ajax({
              type: "post",
              url: "/api/deldeteconcert",    //向后端请求数据的url
              data: {
                  concert_id:concert_id
              },
              success: function (data) {
                  console.log(data);
                  if (data=="0"){
                      $('#delete-success-modal').modal('open');
                      initForm();
                  }

              }
          });
          
      }
      
      function  showForm(data) {
          $("#formbody").empty();
          for(var i=0;i<data.length;i++){
              var dom=' <tr>\n' +
                  '<td style="display: none" class="concert_id">'+data[i].concertid+'</td>\n' +
                  '<td>'+data[i].concertname+'</td>\n' +
                  '<td>'+data[i].singer+'</td>\n' +
                  '<td class="am-hide-sm-only">'+data[i].concertdate +'</td>\n' +
                  '<td class="am-hide-sm-only">'+data[i].concertplace+'</td>\n' +
                  '<td>\n' +
                  '<div class="am-btn-toolbar">\n' +
                  '<div class="am-btn-group am-btn-group-xs">\n' +
                  '<span class="am-btn am-btn-default am-btn-xs am-text-secondary editBtn"><span class="am-icon-pencil-square-o"></span> 编辑</span>\n' +
                  '<span  class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only deleteBtn"><span class="am-icon-trash-o"></span> 删除</span>\n' +
                  '</div>\n' +
                  '</div>\n' +
                  '</td>\n' +
                  '</tr>';
              $("#formbody").append(dom);
          }
          $(".deleteBtn").click(function () {
              var concert_id = $(this).parent().parent().parent().prevAll().filter(".concert_id").text();
              $('#delete-confirm').modal({
                  relatedTarget: this,
                  onConfirm: function(options) {
                      deleteConcert(concert_id.toString());
                  },
                  // closeOnConfirm: false,
                  onCancel: function() {
                      return ;
                  }
              });
          });

          $(".editBtn").click(function () {
              var concert_id = $(this).parent().parent().parent().prevAll().filter(".concert_id").text();
              $.ajax({
                  type: "GET",
                  url: "/api/idconcert",    //向后端请求数据的url
                  data: {
                      concertid:concert_id
                  },
                  success: function (data) {
                      console.log(data);
                      $("#concertname").val(data.concertname);
                      $("#singer").val(data.singer);
                      $("#date").val(data.concertdate);
                      $("#starttime").val(data.concertstarttime);
                      $("#address").val(data.concertplace)
                      $('#update-concert').modal({
                          relatedTarget: this,
                          onConfirm: function(options) {
                              var concertname_ = $("#concertname").val();
                              var singer_ = $("#singer").val();
                              var date_ = $("#date").val();
                              var starttime_ = $("#starttime").val();
                              var address_ = $("#address").val();
                              $.ajax({
                                  type: "PUT",
                                  url: "/api/userdateconcert",    //向后端请求数据的url
                                  data: {
                                      concertid:concert_id,
                                      concertname:concertname_,
                                      singer:singer_,
                                      concertdate:date_,
                                      concertstarttime:starttime_,
                                      concertplace:address_
                                  },
                                  success: function (data) {
                                      if (data){
                                          $('#update-success').modal('open');
                                          initForm();
                                      }else {
                                          $('#update-error').modal('open');
                                      }
                                  }
                              });


                          },
                          // closeOnConfirm: false,
                          onCancel: function() {
                              return ;
                          }
                      });

                  }
              });

          });
      }





        

    });

    //获取浏览器中的Cookie
    function getCookie(cName){
        var cookieString = decodeURI(document.cookie);
        var cookieArray = cookieString.split("; ");
        // console.log(cookieArray.length);
        for(var i = 0; i < cookieArray.length; i++){
            var cookieNum = cookieArray[i].split("=");
            // console.log(cookieNum.toString());
            var cookieName = cookieNum[0];
            var cookieValue = cookieNum[1];

            if(cookieName == cName){
                return cookieValue;
            }
        }
        return false;
    }


})(jQuery);
