(function($) {
    'use strict';
    $(document).ready(function(){

        $("#header").load('../public/header.html');
        var loginname = getCookie("loginname");
        var username = getCookie("username");
        if (username=="管理员"){
            $("#sidebar").load('../public/adminsidebar.html');
        }else {
            $("#sidebar").load('../public/sidebar.html');
        }


        $("#sbuBtn").click(function () {
            var concertname = $("#concertname").val();
            var singer = $("#singer").val();
            var date = $("#date").val();
            var starttime = $("#starttime").val();
            var address = $("#address").val();
            var number_1 = $("#number_1").val();
            var price_1 = $("#price_1").val();
            var number_2 = $("#number_2").val();
            var price_2 = $("#price_2").val();
            var number_3 = $("#number_3").val();
            var price_3 = $("#price_3").val();


            $.ajax({
                type: "POST",
                url: "/api/addconcert",    //向后端请求数据的url
                data: {
                    concertname:concertname,
                    singer:singer,
                    concertdate:date,
                    concertstarttime:starttime,
                    concertplace:address,
                    grade_1:'A',
                    price_1:price_1,
                    seat_1:number_1,
                    grade_2:'B',
                    price_2:price_2,
                    seat_2:number_2,
                    grade_3:'C',
                    price_3:price_3,
                    seat_3:number_3
                },
                success: function (data) {
                    console.log(data);
                    if (date){
                        $('#success-modal').modal('open');
                       $("#concertname").val("");
                       $("#singer").val("");
                       $("#date").val("");
                       $("#starttime").val("");
                       $("#address").val("");
                        $("#number_1").val("");
                        $("#price_1").val("");
                        $("#number_2").val("");
                        $("#price_2").val("");
                        $("#number_3").val("");
                        $("#price_3").val("");
                    }else {
                        $('#error-modal').modal('open');
                    }

                }
            });



        });
    });





    //获取浏览器中的Cookie
    function getCookie(cName){
        var cookieString = decodeURI(document.cookie);
        var cookieArray = cookieString.split("; ");
        // console.log(cookieArray.length);
        for(var i = 0; i < cookieArray.length; i++){
            var cookieNum = cookieArray[i].split("=");
            // console.log(cookieNum.toString());
            var cookieName = cookieNum[0];
            var cookieValue = cookieNum[1];

            if(cookieName == cName){
                return cookieValue;
            }
        }
        return false;
    }


})(jQuery);
