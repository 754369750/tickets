(function($) {
    'use strict';
    var idcard;
    $(document).ready(function(){

        $("#header").load('../public/header.html');

        var loginname = getCookie("loginname");
        var username = getCookie("username");
        if (username=="管理员"){
            $("#sidebar").load('../public/adminsidebar.html');
        }else {
            $("#sidebar").load('../public/sidebar.html');
        }
        $.ajax({
            type: "POST",
            url: "/api/userinfo",    //向后端请求数据的url
            data: {
                userloginname:loginname
            },
            success: function (data) {
                idcard =data.useridcard;
            }
        });

        initData();

        $(".searchBtn").click(function () {

            var keywords = $("#searchtext").val();
            $.ajax({
                type: "GET",
                url: "/api/keywordsconcert",    //向后端请求数据的url
                data: {
                    keywords:keywords
                },
                success: function (data) {
                    if (data.length>0){
                        showForm(data);
                    }
                }
            });

        });



    });

    //获取浏览器中的Cookie
    function getCookie(cName){
        var cookieString = decodeURI(document.cookie);
        var cookieArray = cookieString.split("; ");
        // console.log(cookieArray.length);
        for(var i = 0; i < cookieArray.length; i++){
            var cookieNum = cookieArray[i].split("=");
            // console.log(cookieNum.toString());
            var cookieName = cookieNum[0];
            var cookieValue = cookieNum[1];

            if(cookieName == cName){
                return cookieValue;
            }
        }
        return false;
    }


    //随机数
    function diu_Randomize(b,e){
        if(!b && b!=0 || !e){return "?";}
        return Math.floor( ( Math.random() * e ) + b );
    }
    
    function loadColor() {
        var keyArray = new Array();
        keyArray.push("blue");
        keyArray.push("purple");
        keyArray.push("red");
        keyArray.push("green");
        return keyArray[diu_Randomize(0,3)];

    }
    
    function initData() {
        $.ajax({
            type: "GET",
            url: "/api/allconcert",    //向后端请求数据的url
            data: {},
            success: function (data) {
                if (data.length>0){
                    showForm(data);
                }

            }
        });
    }



    function showForm(data) {
        $("#panelrow").empty();
        for (var i=0;i<data.length;i++){
            var color = loadColor();
            var dom ='<div class="am-u-lg-3 am-u-md-6 am-u-sm-12 " style="float: left">\n' +
                '<div class="dashboard-stat '+color+'">\n' +
                '<div class="visual">\n' +
                '<i class="am-icon-comments-o"></i>\n' +
                '</div>\n' +
                '<div class="details">\n' +
                '<div class="number newstyle">'+data[i].concertname+'</div>\n' +
                '<div class="desc">'+data[i].concertdate+'</div>\n' +
                '</div>\n' +
                '<div class="more_'+data[i].concertid+'">' +
                '<a class="more"> 点击购票\n' +
                '<i class="m-icon-swapright m-icon-white"></i>\n' +
                '</a>\n' +
                '</div>\n' +
                '</div>';
            $("#panelrow").append(dom);
        }

        $(".more").click(function () {
           var classname =  $(this).parent().attr('class');
           var classArray=[];
           classArray = classname.split('_');
            $.ajax({
                type: "GET",
                url: "/api/idconcert",    //向后端请求数据的url
                data: {
                    concertid:classArray[1]
                },
                success: function (data) {
                    if (data){
                        $(".concert_name").text(data.concertname);
                        $("#time").text(data.concertdate+' '+data.concertstarttime);
                        $("#address").text(data.concertplace);
                    }
                }
            });
            $.ajax({
                type: "GET",
                url: "/api/idconcertticket",    //向后端请求数据的url
                data: {
                    concertid:classArray[1]
                },
                success: function (data) {
                    console.log(data)
                    if (data){
                        $("#xibi1").text("席别"+data[0].seatgrade+"("+data[0].seatprice+"元) 剩余:"+data[0].seatleft);
                        if (data[0].seatleft==0){
                            $("#radio_1").attr("disabled", true);
                        }
                        $("#xibi2").text("席别"+data[1].seatgrade+"("+data[1].seatprice+"元) 剩余:"+data[1].seatleft);
                        if (data[1].seatleft==0){
                            $("#radio_2").attr("disabled", true);
                        }
                        $("#xibi3").text("席别"+data[2].seatgrade+"("+data[2].seatprice+"元) 剩余:"+data[2].seatleft);
                        if (data[2].seatleft==0){
                            $("#radio_3").attr("disabled", true);
                        }
                    }else {
                        alert("查询错误")
                    }
                }
            });
            $('#booking-concert').modal({
                relatedTarget: this,
                onConfirm: function(options) {
                    booking(classArray[1]);
                },
                onCancel: function() {
                    return ;
                }
            });
        });
    }
    
    function booking(concert_id) {
        $.ajax({
            type: "post",
            url: "/api/addbooking",    //向后端请求数据的url
            data: {
                concertid:concert_id,
                useridcard:idcard,
                seattype:$("input[name='docInlineRadio']:checked").val()
            },
            success: function (data) {
                console.log(data)
                if (data){
                    $('#booking-concert').modal('close');
                    $('#booking-success-modal').modal('open');
                }

            }
        });

    }

})(jQuery);
